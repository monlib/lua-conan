import os
import shutil

from conans import ConanFile, tools, CMake

class Lua(ConanFile):
    name = 'lua'
    version = '5.3.4'
    license = 'MIT'
    url = 'https://gitlab.com/monlib/lua-conan.git'
    homepage = 'https://www.lua.org/'
    description = ''
    settings = 'os', 'arch', 'build_type', 'compiler'
    generators = 'cmake'
    exports_sources = 'CMakeLists.txt'
    build_requires = 'cmake_installer/[~3.10]@conan/stable'
    options = {
        'bits32': [True, False],
        'forModule': [True, False],
    }
    default_options = (
        'bits32=False',
        'forModule=False',
    )

    def source(self):
        url = 'https://www.lua.org/ftp/lua-5.3.4.tar.gz'
        tools.get(url)
        luafolder = 'lua-5.3.4'
        files = os.listdir(luafolder)
        for f in files: shutil.move(os.path.join(luafolder, f), f)

    def build(self):
        cmake = CMake(self)
        cmake.configure(defs={
            'LUA_32BITS': self.options.bits32,
        })
        cmake.build()

    def package(self):
        self.copy('*lua.lib', dst='lib', keep_path=False)
        self.copy('*.dll', dst='lib', keep_path=False)
        self.copy('*.so', dst='lib', keep_path=False)
        self.copy('*.dylib', dst='lib', keep_path=False)
        self.copy('*.a', dst='lib', keep_path=False)
        headers = ['lua.h', 'luaconf.h', 'lualib.h', 'lauxlib.h', 'lua.hpp']
        for header in headers:
            self.copy(header, dst='include', src='src')

    def package_info(self):
        # Lua modules should not link against the lua core, except on windows.
        # See http://lua-users.org/wiki/BuildingModules.
        shouldBeLinked = self.settings.os == "Windows" or not self.options.forModule
        if shouldBeLinked:
            self.cpp_info.libs = ['lua']
        if self.options.bits32:
            self.cpp_info.defines = ['LUA_32BITS']
